pragma solidity 0.4.0;
pragma experimental ABIEncoderV2;

import {Coin}  from "browser/Coin.sol";


contract Golfer is Coin {
        
    struct GolferInfo {
        uint256 id;
        uint16 level;
        uint8 tournamentsWin;
        uint8 hcp;
        string name;
        string club;
        string typeName;
        
    }
    
    GolferInfo[] public allGolfers;
    
    function addPlayer(uint8 _tournamentsWin, uint8 _hcp, string _name, string _club) public canPaid(50) {
        GolferInfo memory golfer = GolferInfo(allGolfers.length+1, 0, _tournamentsWin, _hcp, _name, _club, "am");
        allGolfers.push(golfer);
        balances[msg.sender] -= 50;
    }
    
    function getGolfer(uint _id) public returns(string, string, uint8, uint8) {
        GolferInfo g = allGolfers[_id];
        return (g.name, g.club, g.hcp, g.tournamentsWin);
    }
    
    function getLevel(uint _id) public returns (string, uint16) {
        GolferInfo g = allGolfers[_id];
        return (g.typeName, g.level);  
    }
    
    function evolution(uint _id) public canPaid(10) {
        GolferInfo g = allGolfers[_id];
        setLevel(g, g.level++);
        balances[msg.sender] -= 10;
    }
    
    function setLevel(GolferInfo g, uint newLevel) public returns (bool ok) {
        if (newLevel > 3) return false;
        if (newLevel == 0) {
            g.typeName = "amateur";
            g.level = 0;
        }
        if (newLevel == 1) {
            g.typeName = "novice";
            g.level = 1;
        }
        if (newLevel == 2) {
            g.typeName = "pro";
            g.level = 2;
        }
        if (newLevel == 3) {
            g.typeName = "legend";
            g.level = 3;
        }
        return true;
    }
}