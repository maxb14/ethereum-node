pragma solidity 0.4.17;


contract Coin {
    
    mapping (address => uint) public balances;
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    
    address public owner;
    string public symbol;
    uint256 public totalSupply;
    uint256 public forSale;
    
    function Coin() {
        totalSupply = 1000;
        forSale = 800;
        owner = msg.sender;
        balances[owner] = 200;
        symbol = "ORM";
    }
    
    function send(address receiver, uint amount) public returns (bool ok) {
        if (balances[msg.sender] < amount) return false;
        balances[msg.sender] -= amount;
        balances[receiver] += amount;
        Transfer(msg.sender, receiver, amount);
        return true;
    }
    
    function buyCoin(uint amount) public returns (bool ok) {
        if (amount > forSale) return false;
        forSale -= amount;
        balances[msg.sender] += amount;
        return true;
    }

    function getForSale() public returns (uint) {
        return forSale;
    }
    
    function getBalance(address addr) public returns (uint) {
        return balances[addr];
    }
    
    function getTotalSupply() public returns (uint) {
        return totalSupply;
    }
    
    function getSymbol() public returns (string) {
        return symbol;
    }

    modifier canPaid(uint _price) {
        require(balances[msg.sender] > _price);
        _;
    }
}
